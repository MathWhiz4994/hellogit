#include <vector>
#include <string>
#include <iostream>
#include <cstring>

using namespace std;

int main(){
	vector<int> nums;
	int new_num;
	char choose_another;
	
	cout << "Enter a value and press return." << '\n';
	cin >> new_num;
	
	nums.push_back(new_num);
	
	cout << "The number you have chosen is " << new_num << ".\n" << "Would you like to choose another value? (y/n)";
	cin >> choose_another;
	
	while (choose_another == 'y'){
		cout << "Great! Enter another value and press return.\n";
		cin >> new_num;
		
		nums.push_back(new_num);
		
		cout << "The numbers you have chosen are ";
		
		for (auto i=0; i < nums.size(); i++) {
			cout << nums[i] << " ";
		}
		cout << "\nWould you like to choose another value? (y/n)";
		cin >> choose_another;
	}
	
	cout << "Done so soon?\nThe numbers you chose were ";
	
	for (auto i=0; i < nums.size(); i++){
		cout << nums[i] << " ";
	}
	
	return 0;
}
